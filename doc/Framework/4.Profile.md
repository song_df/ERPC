
# 目录
- [一、框架配置文件](#1)
	- [1.1 全局配置](#1.1)
	- [1.2 进程配置](#1.2)
	- [1.3 特殊配置说明](#1.3)
- [二、日志配置文件](#2)
	- [2.1 总体概述](#2.1)
	- [2.2 输出格式比较](#2.2)
	- [2.3 日志输出规则](#2.3)

在快速入门中，我们所有的例程均使用的相同的配置文件，为了简化使用，所有例程使用过程中均没有展现配置文件。接下来详细介绍一下配置文件。

ERPC总共包含两个配置文件：框架配置文件rpc.json和日志配置文件。

当动态修改配置文件（rpc.json，log.conf）时，请采用复制->修改->覆盖的方式，因为直接修改会触发rename信号，导致无法长久监控文件状态。

# <a name="1">一、框架配置文件</a>

框架配置文件采用JSON的格式进行存储，我们以实例中的配置文件为例进行讲解。
```
$ cat conf/rpc.json 
{
	"GlobalConfiguration": {
		"GlobalFixThreads":5,
		"GlobalDynThreads":8,
		"GlobalMaxWorkQueue":300,
		"GlobalCycleTimerPeriod":"15s",
		"GlobalConnectTimeout":"80ms",
		"GlobalResponseTimeout":"50ms",
		"GlobalLoggerConfigPath":"/work/study/Module/ERPC/source/conf/log.conf"
	},
	"ProcessConfiguration": [
		{"service": {
			"FixThreads":2,
			"DynThreads":3,
			"MaxWorkQueue":50,
			"ConnectTimeout":"50ms",
			"ResponseTimeout":"30ms",
			"ListenPort":0,
			"IPAddress":"/tmp/s_media_service",
			"Deployment":["hello", "Key"]
		}},
		{"app": {
			"ListenPort":8989,
			"IPAddress":"127.0.0.1",
			"Deployment":["logic", "IO"]
		}}
	]
}
```

整体配置分为两大块：
- 全局配置：包括固定线程数、动态线程数、最大工作队列数、循环定时器周期、连接超时、响应超时；
- 进程配置：为一个JSON数组，每一项即一个进程的配置，包括固定线程数、动态线程数、最大工作队列数、循环定时器周期、连接超时、响应超时，以及进程的监听端口、IP地址和模块的部署情况。

## <a name="1.1">1.1 全局配置</a>

全局配置均已Global开头，包括固定线程数、动态线程数、最大工作队列数、循环定时器周期、连接超时、响应超时：
- 固定线程数、动态线程数、最大工作队列数为整型数据（int类型），根据系统判断最大值；
- 循环定时器周期单位可以为S和Min，最终在系统中换算成S的单位，最大值为系统整型数据的最大值；
- 连接超时和响应超时单位可为ms、S和Min，最终在系统中换算成ms的单位，最大值为系统整型数据的最大值。

全局配置信息最终会转化为进程的默认配置，也就是说进程可以不配置固定线程数、动态线程数、最大工作队列数、循环定时器周期、连接超时、响应超时信息，那么就会默认使用全局的配置信息。

## <a name="1.2">1.2 进程配置</a>

进程配置整体为一个JSON数组，每一项对应一个进程的配置。进程配置中也有全局配置中的固定线程数、动态线程数、最大工作队列数、循环定时器周期、连接超时、响应超时，只是不以Global开头。除此之外，每个进程还包括网络配置信息和部署配置信息。

1. 网络配置信息

网络配置信息包括监听端口号和IP地址信息：
- 当端口号为0时，启动LocalSocket模式，IPAddress对应一个绝对路径的文件；
- 当端口号非0时，启动网络模式：IPAddress若为127.0.0.1，则使用环形Socket；若为INADDR_ANY或其他值，则使用标准Socket通信；

说明：**对于进程内部的配置建议都使用LocalSocket模式（或者环形Socket也行），速度会更快也更稳定。只有需要与外交互的进程才使用标准Socket通信，但本机内可使用INADDR_ANY，固定端口即可，本机外的进程，才需要详细的IP地址和端口信息。**

2. 部署配置

进程是部署的最小单位，模块是被部署的最小单位，模块只有部署在进程中才能运转。部署配置项也是一个JSON数组，每一项对应一个模块名。

说明：**在我们编写代码实现注册服务、创建观察者、远程调用等功能时，需要保证接口传入模块名与部署的模块名保持一致（区分大小写），否则ERPC无法找到对应的模块**。

## <a name="1.3">1.3 特殊配置说明</a>

1. 连接超时的优先顺序是：进程自己的ConnectTimeout > 全局配置的GlobalConnectTimeout > 软件宏定义的RPC_CONNECT_TIMEOUT；
2. 通信超时的优先顺序是：
	- 当传入参数为NULL，表示使用系统默认，则优先顺序是：进程自己的ResponseTimeout > 全局配置的GlobalResponseTimeout
	- 当传入参数非NULL，但值为{0, 0}的情况，表示长等（永久等待），会占用一个代理（线程），不建议使用，可使用observer模式实现对应的功能；
	- 当传入参数非NULL，且值有效，则会使用用户指定的等待响应时长；
3. 周期定时器的优先顺序是：进程自己的CycleTimerPeriod > 全局配置的GlobalCycleTimerPeriod > 软件宏定义的RPC_TIMER_PERIOD；并且定时器的单位是秒S或Min。

# <a name="2">二、日志配置文件</a>

## <a name="2.1">2.1 总体概述</a>

ERPC使用的是C语言开源日志库Zlog，详细的使用方法和配置文件详细信息可见[官方网站](http://hardysimpson.github.io/zlog/)和[使用手册](http://hardysimpson.github.io/zlog/UsersGuide-CN.html)

这里为了简化使用，将日志库的配置标准化了，用户只需要根据自身需要，**选择使用什么日志格式、配置输出方式即可**。下面还是以实例的日志配置文件来讲解：
```
$ cat conf/log.conf 
[global]
strict init = true
buffer min  = 1024
buffer max  = 2MB
rotate lock file = /tmp/zlog.lock
default format = "%d(%F %X).%msms %-6V %c[%p](%f:%U():%L) - %m"

[formats]
null      = ""
print     = "%m"
simple    = "%d(%F %X) %-6V (%f:%U():%L) - %m"
normalms  = "%d(%F %X).%msms %-6V %c[%p](%f:%U():%L) - %m"
normalus  = "%d(%F %X).%usus %-6V %c[%p](%f:%U():%L) - %m"
complexms = "%d(%F %X).%msms %-6V %H %c[%p:%t](%f:%U():%L) - %m"
complexus = "%d(%F %X).%usms %-6V %H %c[%p:%t](%f:%U():%L) - %m"

mdc_format = "%d(%F %X).%msms %-6V %c[%p](%f:%U():%L) [%M(myMDC)] - %m"

[rules]
*.*        >stdout;
#*.*        >stderr;
#!.*        >stderr;

#*.debug    >stderr;
#*.=debug   >stderr;
#*.!debug   >stderr;

#*.info     >stderr;
#*.notice   >stderr;
#*.warn     >stderr;
#*.error    >stderr;
#*.fatal    >stderr;

#*.*        >stderr;simple
#*.*        >stderr;normalms
#*.*        >stderr;complexms
#*.*        >stderr;mdc_format

#*.*        "./aa.%d(%F).log";
#*.*        "./aa.%d(%F).log";normalms
#*.*        -"./aa.%d(%F).log";
#*.*        "./aa.log", 1KB * 5;
#*.*        "./aa.log", 10MB * 5;
#*.*        "./aa.log", 10MB * 5;normalms
```

这里只对部分配置进行说明：
- 日志配置文件中，#开始的行为注释行，不起作用；
- global为全局配置，formats为日志格式配置，rules为输出规则，用户只需要关心输出规则即可；
- 不指定日志格式配置时，使用全局配置中的default format输出规则，如下：
'''
2019-09-23 15:49:40.723ms ERROR  app[4930](rpc_proxy.c:rpc_proxy_wait():169) - RPC : proxy wait timeout!
'''
- rules规则可以配置不同的输出等级（debug、info、notice、warn、error和fatal），不同等级的日志输出到不同的位置（stdout、stderr或文件），还可指定输出的格式（null、print、simple、normalms、normalus、complexms、complexus）；对于输出到文件，还可以限定单个文件的大小，以及转存的方法（按大小或按日期）。

## <a name="2.2">2.2 输出格式比较</a>

相对default format格式输出如下日志：
'''
2019-09-23 15:49:40.723ms ERROR  app[4930](rpc_proxy.c:rpc_proxy_wait():169) - RPC : proxy wait timeout!
'''

formats中的其他配置输出格式分别为：
- null输出格式为不做任何输出；
- print输出格式为只输出应用打印的日志，没有日期、时间、错误等级、进程信息和源码定位信息：
```
RPC : proxy wait timeout!
```
- simple输出格式的输出信息如下，时间上少了ms信息，也没有进程信息：
```
2019-09-23 16:01:42 ERROR  (rpc_proxy.c:rpc_proxy_wait():169) - RPC : proxy wait timeout!
```
- normalms输出格式如下，与默认配置一致：
```
2019-09-23 16:03:49.836ms ERROR  app[7435](rpc_proxy.c:rpc_proxy_wait():169) - RPC : proxy wait timeout!
```
- normalus输出格式如下，相对normalms，时间改为us级别：
```
2019-09-23 16:06:09.220348us ERROR  app[7696](rpc_proxy.c:rpc_proxy_wait():169) - RPC : proxy wait timeout!
```
- complexms输出格式如下，增加输出了用户信息和线程信息：
```
2019-09-23 16:07:12.829ms ERROR  konishi app[7823:18dbd740](rpc_proxy.c:rpc_proxy_wait():169) - RPC : proxy wait timeout!
```
- complexms输出格式如下，相对complexms，时间改为us级别：
```
2019-09-23 16:08:59.244583ms ERROR  konishi app[8031:f550d740](rpc_proxy.c:rpc_proxy_wait():169) - RPC : proxy wait timeout!
```

## <a name="2.3">2.3 日志输出规则</a>

- 所有分类、所有等级的日志按照默认格式全部输出到标准输出：
```
*.*        >stdout;
```
- 所有分类、所有等级的日志按照默认格式全部输出到错误输出：
```
*.*        >stderr;
```
- Media分类、所有等级的日志按照默认格式全部输出到标准输出：
```
Media.*    >stdout;
```
- Media分类、Info等级及以上的日志按照默认格式输出到标准输出：
```
Media.info  >stdout;
```
- Media分类、只有Error等级的日志按照默认格式输出到标准输出：
```
Media.=error  >stdout;
```
- Media分类、除debug等级外的日志按照默认格式输出到错误输出：
```
Media.!debug  >stderr;
```
- 未匹配分类、所有日志按照默认格式输出到标准输出：
```
!.*        >stdout;
```
- 所有分类、所有等级的日志按照simple格式输出到标准输出：
```
*.*        >stdout;simple
```
- Media分类、所有等级的日志按照normalms格式输出到错误输错：
```
Media.*        >stderr;normalms
```
- 所有分类、所有等级的日志输出到NULL，也就是不输出：
```
*.*        "/dev/null"
```
- 将Media分类、所有等级的日志，按照normalms格式输出到media.log中：
```
Media.*     "./media.log";normalms
```
- 将Media分类、所有等级的日志，按照normalms格式实时输出到media.log中：
```
Media.*     -"./media.log";normalms
```
- 将Media分类、所有等级的日志，按照normalms格式输出到media.log中，并限定文件大小为1M，至多5个文件，按照下标自动转档：
```
Media.*     "./media.log"，1M * 5;normalms
```
- 将Media分类、所有等级的日志，按照normalms格式输出到media.log中，并按照日期进行转档：
```
Media.*     "./media.%d(%F).log";normalms
```

注意：对于嵌入式设备来讲，Flash的存储有限，不建议采用日期转档，应该使用限定文档大小进行转档，并且只输出严重的错误等级（调试阶段除外）。
