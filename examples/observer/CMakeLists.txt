cmake_minimum_required(VERSION 3.8)

add_executable(app3 app.c)
add_executable(service3 service.c)
target_link_libraries(app3 ${Libraries})
target_link_libraries(service3 ${Libraries})
