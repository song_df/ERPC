#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>

#include "erpc.h"

void *business(void *arg)
{
    char *message = NULL;
    cJSON *request = NULL;
    cJSON *response = NULL;

    while(1)
    {
        sleep(3);
        request = cJSON_CreateObject();
        if(NULL == request)
            continue;

        cJSON_AddStringToObject(request, "hello", "I'm helloworld app.");
        if(0 != erpc_service_proxy_call("hello", "helloworld", request, &response, NULL))
        {
            cJSON_Delete(request);
            request = NULL;
            continue;
        }

        if(response)
        {
            message = cJSON_Print(response);
            printf("result: %s\n", message);
            free(message);
            cJSON_Delete(response);
        }
    }
}

pthread_t business_id;

int main(void)
{
    erpc_framework_init("app");

    if(0 != pthread_create(&business_id, NULL, business, NULL))
        return -1;

    return erpc_framework_loop(ERPC_LOOP_DEFAULT);
}

